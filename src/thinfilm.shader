Shader "Physical/thinfilm"
{
    Properties
    {
        _DiffuseTex ("Diffuse Texture", 2D) = "white" {}
        _MaskTex ("Mask Texture", 2D) = "white" {}
        _SurfNormal ("Surface Normals", 2D) = "white" {}
        _SurfScale ("Surface Scale", Float) = 4.0
        _SurfRoughness ("Surface Roughness", Float) = 0.5
        _ReflectionMap ("Reflection Map", CUBE) = "white" {}
        _ThicknessMap ("Thickness Map", 2D) = "" {}
        _FilmThickness ("Film Thickness (nm)", Range(0, 2000)) = 450
        _Roughness ("Roughness", Range(0, 1)) = 0.25
        _Transparency ("Transparency", Range(0, 1)) = 0.0
        _Brightness ("Brightness", Range(0, 100)) = 1.0
        _DiffuseBrightness ("Diffuse Brightness", Range(0, 1)) = 1.0
        _AmbientLight ("Ambient Light", Range(0, 1)) = 0.0
    }
    SubShader
    {
        Tags {
            "RenderType"="Transparent"
            "Queue"="Transparent"
            "LightMode"="ForwardBase"
        }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"
            #include "cie.cginc"

            #pragma multi_compile_fwdbase

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 viewDir : TEXCOORD1;
                float3 normal : NORMAL;
                float4 pos : TEXCOORD2;
                float3 ambient : TEXCOORD3;
                SHADOW_COORDS(4)
                float3 worldNormal : TEXCOORD5;
                float3 objectPos : TEXCOORD6;
                float3 worldPos : TEXCOORD7;
            };

            #define PI 3.1415926535897932384626433832795
            #define E 2.71828

            #define xUnitVec3 float3(1.0, 0.0, 0.0)
            #define yUnitVec3 float3(0.0, 1.0, 0.0)
            
            sampler2D _DiffuseTex;
            sampler2D _MaskTex;
            sampler2D _SurfNormal;
            float _SurfScale;
            float _SurfRoughness;
            samplerCUBE _ReflectionMap;
            sampler2D _ThicknessMap;

            float _FilmThickness;
            float _Roughness;
            float _Transparency;
            float _Brightness;
            float _DiffuseBrightness;
            float _AmbientLight;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.viewDir = -WorldSpaceViewDir(v.vertex);
                o.normal = v.normal;
                o.worldNormal = UnityObjectToWorldNormal(v.normal);

                o.pos = o.vertex;
                o.ambient = ShadeSH9(float4(o.worldNormal, 1));
                TRANSFER_SHADOW(o)

                o.objectPos = v.vertex;
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }
    
            float gaussian(float x, float mean, float stddev, float scale) {
                float a = 1. / (stddev * sqrt(2 * 3.1415926));
                float b = -1./2. * pow((x - mean) / stddev, 2);
                return saturate(scale * a * exp(b));
            }

            float sum(float3 x) {
                return x.r + x.g + x.b;
            }

            float4 setAxisAngle(float3 axis, float rad) {
                rad = rad * 0.5;
                float s = sin(rad);
                return float4(s * axis[0], s * axis[1], s * axis[2], cos(rad));
            }

            float4 rotationTo(float3 a, float3 b) {
                float vecDot = dot(a, b);
                float3 tmpvec3 = float3(0, 0, 0);
                if (vecDot < -0.999999) {
                    tmpvec3 = cross(xUnitVec3, a);
                    if (length(tmpvec3) < 0.000001) {
                    tmpvec3 = cross(yUnitVec3, a);
                    }
                    tmpvec3 = normalize(tmpvec3);
                    return setAxisAngle(tmpvec3, PI);
                } else if (vecDot > 0.999999) {
                    return float4(0,0,0,1);
                } else {
                    tmpvec3 = cross(a, b);
                    float4 _out = float4(tmpvec3[0], tmpvec3[1], tmpvec3[2], 1.0 + vecDot);
                    return normalize(_out);
                }
            }

            float4 multQuat(float4 q1, float4 q2) {
                return float4(
                q1.w * q2.x + q1.x * q2.w + q1.z * q2.y - q1.y * q2.z,
                q1.w * q2.y + q1.y * q2.w + q1.x * q2.z - q1.z * q2.x,
                q1.w * q2.z + q1.z * q2.w + q1.y * q2.x - q1.x * q2.y,
                q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z
                );
            }

            float3 rotateVector(float4 quat, float3 vec) {
                float4 qv = multQuat(quat, float4(vec, 0.0));
                return multQuat(qv, float4(-quat.x, -quat.y, -quat.z, quat.w)).xyz;
            }

            float3 gaussian(float x, float4 mean, float4 stddev, float4 scale) {
                float4 a = 1. / (stddev * sqrt(2 * 3.1415926));
                float4 b = -1./2. * pow((float4(x, x, x, x) - mean) / stddev, 2);
                float4 v = scale * a * exp(b);
                float3 res = v.rgb + float3(v.w/2, 0, v.w/2);
                return saturate(res);
            }

            float3 intersectLinePlane(float3 p0, float3 p1, float3 p_co, float3 p_no) {
                float3 u = p1 - p0;
                float d = dot(p_no, u);

                if (abs(d) > 1e-6) {
                    float3 w = p0 - p_co;
                    float fac = -dot(p_no, w) / d;
                    u *= fac;
                    return p0 + u;
                }

                return float3(0, 0, 0);
            }

            float3 calcNormal (v2f i) {
                float3 surfNormal = normalize(tex2D(_SurfNormal, i.uv * _SurfScale) * 2 - 1);
                float3 worldNormal = normalize(i.worldNormal);
                float4 quat = rotationTo(worldNormal, float3(0, 0, -1));
                float3 normal = -normalize(rotateVector(quat, normalize(surfNormal)));
                normal += (normal - worldNormal) * _SurfRoughness;
                normal = normalize(normal);
                return normal;
            }

            float srand (float x, float y) {
                float a = cos(x * 12.9898f + y * 4.1414f) * 43758.5453f;
                return a-floor(a);
                
            }

            float rand(float2 co){
                return frac(sin(dot(co, float2(12.9898, 78.233))) * 43758.5453);
            }

            float noise(float2 st) {
                float2 i = floor(st);
                float2 f = frac(st);

                float a = rand(i);
                float b = rand(i + float2(1.0, 0.0));
                float c = rand(i + float2(0.0, 1.0));
                float d = rand(i + float2(1.0, 1.0));

                float2 u = f*f*(3.0-2.0*f);

                return lerp(a, b, u.x) +
                        (c - a)* u.y * (1.0 - u.x) +
                        (d - b) * u.x * u.y;
            }

            float fbm (float2 st) {
                float value = 0.0;
                float amplitude = .5;
                float wavelenuency = 0.;
                for (int i = 0; i < 6; i++) {
                    value += amplitude * noise(st);
                    st *= 2.;
                    amplitude *= .5;
                }
                return value;
            }

            float3 randv (float3 seed){
                float a = srand(seed.x, seed.y);
                float b = srand(seed.z, seed.y);
                float c = srand(seed.x, seed.z);
                float d = srand(a, b);
                float e = srand(b, c);
                float f = srand(d, e);
                float3 r = float3(d,e,f);
                return r;
            }

            float3 random_bounce (float3 normal, float3 r) {
                float3 x_axis = float3(1., 0., 0.);
                float3 z_axis = float3(0., 0., 1.);
                float3 y_axis = float3(0., 1., 0.);
                float x_rand = r.x * 1;
                float z_rand = r.z * 1;
                float4 x_quat = {x_axis.x, x_axis.y, x_axis.z, PI/2.*x_rand};
                float4 z_quat = {z_axis.x, z_axis.y, z_axis.z, PI/2.*z_rand};
                float3 dir = y_axis;
                dir = normalize(rotateVector(x_quat, dir));
                dir = normalize(rotateVector(z_quat, dir));
                float4 quat = rotationTo(normal, y_axis);
                dir = normalize(rotateVector(quat, dir));
                dir = lerp(normal, dir, smoothstep(0, 1, r.x));
                return dir;
            }

            float4 frag (v2f i) : SV_Target
            {
                float3 N = calcNormal(i);
                float3 I_ = normalize(i.viewDir);

                float raw_filmNoise = fbm((i.worldPos.xz + float2(0,0)) / 4);
                float filmNoise = saturate(saturate(max(0.2, smoothstep(0, 1, raw_filmNoise * 0.75)) - 0.2) * 10);

                float refrIndex = 1.05;
                float filmThickness = _FilmThickness;

                float3 thickness_map = tex2D(_ThicknessMap, i.uv);
                filmThickness = thickness_map.x * filmThickness;
                float roughness = _Roughness;

                float4 xyza = float4(0, 0, 0, 0);
                float3 surf_color = tex2D(_DiffuseTex, i.uv);

                float3 refl = reflect(I_, N);
                float reflections = smoothstep(0, 1, texCUBE(_ReflectionMap, refl).x);

                int rays = 16;
                int steps = 32;

                float3 absorp = rgbToXYZ(_AmbientLight);

                for (int n = 0; n < rays; n++) {
                    float3 seed = float3((n + 1) * 701, (n + 1) * 9973, (n + 1) * 487) + i.worldPos * 31.;
                    float3 r = randv(seed);
                    float3 rand_I = reflect(random_bounce(N, r), N);
                    float3 I = lerp(I_, rand_I, roughness);

                    float3 refl = reflect(I, N);

                    float3 refr = refract(I, N, refrIndex);
                    refr = length(refr) == 0 ? I : refr;

                    float3 a = N * filmThickness;
                    float3 b = intersectLinePlane(a, a + refr, float3(0, 0, 0), N);
                    float3 refl_refr = reflect(refr, N);
                    float3 c = b + refl_refr * distance(a, b);
                    float theta_1 = acos(dot(I, N));
                    float dist_a_to_d = sin(theta_1) * distance(a, c);

                    float filmSurf_ray_len = dist_a_to_d;

                    float interf;
                    for (float wavelen = 400.; wavelen < 800.; wavelen += (400. / steps)) {
                        
                        int bounces = 1;
                        float objSurf_ray_len = distance(a, b) + distance(b, c) * bounces;
                        if (refrIndex > 1.0) {
                            objSurf_ray_len += wavelen * 0.5;
                        }
                        float dist = (objSurf_ray_len - filmSurf_ray_len);

                        float filmSurf_ray_phase = (filmSurf_ray_len % wavelen) / wavelen;
                        float objSurf_ray_phase = (objSurf_ray_len % wavelen) / wavelen;
                        float interf = 4 * abs(abs(filmSurf_ray_phase - objSurf_ray_phase) - 0.5);

                        float4 inv_absorption = float4(surf_color, (surf_color.r + surf_color.b) / 2);
                        xyza += interf * reflections * float4(wavelenToXYZ(wavelen) * absorp, 1);
                    }
                }
                xyza /= rays;
                xyza.a /= steps;
                float4 color = float4(xyzToRGB(xyza.xyz), xyza.a);

                float film_mask = saturate(color.a + (1 - _Transparency));

                reflections *= color.a;

                float color_mask = tex2D(_MaskTex, i.uv).x > 0.1;

                return float4(_Brightness * color.rgb, film_mask * color_mask);
            }
            ENDCG
        }
    }
}
