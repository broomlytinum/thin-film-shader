# Thin Film Shader

![A computer-rendered bubble, iridescent ball of scales and rainbow bacteria demonstrating a thin film interference shader.](/images/assorted.png)

A physically-based Unity shader (HLSL) modeled after the thin film interference phenomenon.

## Leptothrix discophora

By supplying a film thickness map, one can replicate the appearance of bacteria such as Leptothrix discophora:

<img src="/images/leptothrix.png" width="600" height="auto" alt="A close-up render of rainbow bacteria demonstrating the thin film inference shader.">
<br>
<img src="/images/thickness_map.png" width="600" height="auto" alt="A thickness map for the bacteria render, using light to dark regions to represent film thickness at each point.">